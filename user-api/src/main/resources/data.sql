INSERT INTO user(id, username, password, bio, image_url) VALUES (1, 'jim', 'password', 'Bio', 'imageurl');
INSERT INTO user(id, username, password, bio, image_url) VALUES (2, 'daphne', 'password', 'Bio', 'imageurl');
INSERT INTO user(id, username, password, bio, image_url) VALUES (3, 'john', 'password', 'Bio', 'imageurl');
INSERT INTO user(id, username, password, bio, image_url) VALUES (4, 'jane', 'password', 'Bio', 'imageurl');
INSERT INTO user(id, username, password, bio, image_url) VALUES (5, 'barry', 'password', 'Bio', 'imageurl');

INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (1, 2);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (1, 3);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (1, 4);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (1, 5);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (2, 1);

INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (2, 1);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (3, 1);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (4, 1);
INSERT INTO user_follows(follows_id, followed_bys_id) VALUES (5, 1);



