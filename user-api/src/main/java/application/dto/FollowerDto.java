package application.dto;

import application.domain.User;

public class FollowerDto {
  private Long id;
  private String username;
  private String imageUrl;

  public FollowerDto(Long id, String username, String imageUrl) {
    this.id = id;
    this.username = username;
    this.imageUrl = imageUrl;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public static FollowerDto fromUser(User follower) {
    return new FollowerDto(
      follower.getId(),
      follower.getUsername(),
      follower.getImageUrl()
    );
  }
}
