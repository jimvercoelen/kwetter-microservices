package application.dto;

import application.domain.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserDto {
  private Long id;
  private String username;
  private String password;
  private String web;
  private String bio;
  private String imageUrl;
  private List<FollowerDto> follows;
  private List<FollowerDto> followedBy;

  public UserDto () {

  }

  public UserDto(Long id, String username, String password, String web, String bio, String imageUrl) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.web = web;
    this.bio = bio;
    this.imageUrl = imageUrl;
  }

  public UserDto(Long id, String username, String password, String web, String bio,
                 String imageUrl, List<FollowerDto> follows, List<FollowerDto> followedBy) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.web = web;
    this.bio = bio;
    this.imageUrl = imageUrl;
    this.follows = follows;
    this.followedBy = followedBy;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getBio() {
    return bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public List<FollowerDto> getFollows() {
    return follows;
  }

  public void setFollows(List<FollowerDto> follows) {
    this.follows = follows;
  }

  public List<FollowerDto> getFollowedBy() {
    return followedBy;
  }

  public void setFollowedBy(List<FollowerDto> followedBy) {
    this.followedBy = followedBy;
  }

  public static UserDto fromUser(User user) {
    return new UserDto(
      user.getId(),
      user.getUsername(),
      user.getPassword(),
      user.getWeb(),
      user.getBio(),
      user.getImageUrl(),
      user.getFollows().stream().map(FollowerDto::fromUser).collect(Collectors.toList()),
      user.getFollowedBys().stream().map(FollowerDto::fromUser).collect(Collectors.toList())
    );
  }

  // This one will return a UserDto without followers and following
  public static UserDto fromUserWithoutFollowers(User user) {
    return new UserDto(
      user.getId(),
      user.getUsername(),
      user.getPassword(),
      user.getWeb(),
      user.getBio(),
      user.getImageUrl()
    );
  }
}
