package application.rest;

import application.domain.User;
import application.dto.UserDto;
import application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserRest {

  @Autowired
  private UserRepository userRepository;

  @PostMapping("/auth")
  public ResponseEntity authenticate(@RequestBody User user) {
    UserDto userDto = null;
    HttpStatus status = HttpStatus.OK;

    try {
      userDto = UserDto.fromUser(userRepository.authenticate(user.getUsername(), user.getPassword()));
    } catch (Exception e) {
      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity
      .status(status)
      .body(userDto);
  }

  @PostMapping("/sign-up")
  public ResponseEntity signUp(@RequestBody User user) {
    UserDto userDto = null;
    HttpStatus status = HttpStatus.OK;

    try {
      userDto = UserDto.fromUser(userRepository.save(user));
    } catch (Exception e) {
      e.printStackTrace();

      if (e instanceof DataIntegrityViolationException) {   // Caused by user already exists
        status = HttpStatus.CONFLICT;
      } else {
        status = HttpStatus.INTERNAL_SERVER_ERROR;
      }
    }

    return ResponseEntity
      .status(status)
      .body(userDto);
  }

  @PutMapping
  public ResponseEntity update(@RequestBody User user) {
    UserDto userDto = null;
    HttpStatus status = HttpStatus.OK;

    try {
      userDto = UserDto.fromUser(userRepository.save(user));
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity
      .status(status)
      .body(userDto);
  }

  @DeleteMapping("/{userId}")
  public ResponseEntity delete(@PathVariable("userId") String userId) {
    HttpStatus status = HttpStatus.OK;

    try {
      userRepository.deleteById(Long.valueOf(userId));
    } catch (Exception e) {
      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity.status(status).build();
  }
}
