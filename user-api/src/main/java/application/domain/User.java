package application.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true, nullable = false)
  private String username;

  @Column(nullable = false)
  private String password;

  private String web;

  private String imageUrl;

  private String bio;

  @ManyToMany(mappedBy = "follows", cascade = CascadeType.PERSIST)
  private List<User> followedBys = new ArrayList<>();

  @ManyToMany(cascade = CascadeType.PERSIST)
  private List<User> follows = new ArrayList<>();

  public User() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getBio() {
    return bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

  public List<User> getFollowedBys() {
    return followedBys;
  }

  public void setFollowedBys(List<User> followedBys) {
    this.followedBys = followedBys;
  }

  public List<User> getFollows() {
    return follows;
  }

  public void setFollows(List<User> follows) {
    this.follows = follows;
  }
}
