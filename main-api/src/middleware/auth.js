const token = require('../utils/token')

/**
 * Authentication middleware for checking JWT token
 */
function auth (req, res, next) {
  const tokenHeader = req.headers.token

  if (!tokenHeader) {
    return res.sendStatus(403)
  }

  token.verify(tokenHeader, (error, result) => {
    if (error) {
      return res.sendStatus(403)
    }

    req.user = {
      id: result.userId
    }

    next()
  })
}

module.exports = auth
