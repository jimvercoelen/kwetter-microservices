const router = module.exports = require('express').Router()
const request = require('request')
const _ = require('lodash')
const querystring = require('querystring')
const async = require('async')
const config = require('../config')
const authTokenHelper = require('../utils/authTokenHelper')

router.post('/main', (req, res, next) => {
  const { body: { username, password } } = req;

  // Request auth user
  request.post(`${config.USER_API_URL}/auth`, {
    body: { username, password },
    json: true
  }, (error, response, auth) => {
    if (error) {
      return next(error)
    }

    // Request auth's own posts
    request.get(`${config.POSTS_API_URL}/${auth.id}`, {
      json: true
    }, (error, response, ownPosts) => {
      if (error) {
        return next(error)
      }

      auth.ownPosts = ownPosts

      const followersQueryString = querystring.stringify({ followers: _.map(auth.follows, f => f.id) })

      // Request auth's timeline posts
      request.get(`${config.POSTS_API_URL}/timeline/${auth.id}?${followersQueryString}`, {
        json: true
      }, (error, response, timelinePosts) => {
        if (error) {
          return next(error)
        }

        auth.timelinePosts = timelinePosts

        return authTokenHelper.createSendTokenObject({
          user: auth,
          res
        })
      })
    })
  })
})



