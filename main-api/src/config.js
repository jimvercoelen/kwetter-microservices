const pkg = require('../package.json')

const NAME = pkg.name
const VERSION = pkg.version

const PORT = 8081
const URL = `http://localhost:${PORT}`

const USER_API_URL = 'http://localhost:8082/user'
const POSTS_API_URL = 'http://localhost:8083/posts'


const JWS = {
  SECRET: 'JeKkMoeder<3',
  EXPIRE: 86400 // 24 hours
}

module.exports = {
  NAME,
  VERSION,

  PORT,
  URL,
  USER_API_URL,
  POSTS_API_URL,

  JWS
}
