const token = require('./token')
const config = require('../config')

/**
 * Will create a JWS token for an object and returns it including the object in JSON format.
 *
 * See middleware/auth
 */
function createSendTokenObject ({ user, res }) {
  res.json({
    token: token.create(user.id),
    expiresIn: config.JWS.EXPIRE,
    user
  })
}

module.exports = {
  createSendTokenObject
}
