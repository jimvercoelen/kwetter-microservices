const jwt = require('jsonwebtoken')
const config = require('../config')

const create = (userId) => jwt.sign({ userId }, config.JWS.SECRET, { expiresIn: config.JWS.EXPIRE })
const verify = (token, callback) => jwt.verify(token, config.JWS.SECRET, callback)

module.exports = {
  create,
  verify
}
