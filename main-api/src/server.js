const path = require('path')
const fs = require('fs')
const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const logger = require('./utils/logger')
const errorHandler = require('./middleware/errorHandler')
const server = module.exports = express()

server.use(logger.requestLogger)
server.use(helmet())
server.use(cors())
server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())

const routesPath = path.join(__dirname, './routes')
const routes = fs.readdirSync(routesPath)

server.use(express.static('public'))

routes.forEach(route => {
  server.use(require(path.join(routesPath, route)))
})

server.use(logger.errorLogger)
server.use(errorHandler)
