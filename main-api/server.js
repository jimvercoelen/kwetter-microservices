const async = require('async')
const server = require('./src/server')
const config = require('./src/config')
const logger = require('./src/utils/logger')

function initializeServer (callback) {
  server.listen(config.PORT, callback)
}

async.parallel([
  initializeServer
], (error) => {
  if (error) {
    return logger.error(error)
  }

  logger.info(`Server is running at ${config.URL}`)
})
