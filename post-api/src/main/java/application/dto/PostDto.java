package application.dto;

import application.domain.Post;

public class PostDto {
  private Long id;
  private String content;
  private String dateTime;
  private Long posterId;

  public PostDto(Long id, String content, String dateTime, Long posterId) {
    this.id = id;
    this.content = content;
    this.dateTime = dateTime;
    this.posterId = posterId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDateTime() {
    return dateTime;
  }

  public void setDateTime(String dateTime) {
    this.dateTime = dateTime;
  }

  public Long getPosterId() {
    return posterId;
  }

  public void setPosterId(Long posterId) {
    this.posterId = posterId;
  }
  public static PostDto fromPost(Post post) {
    return new PostDto(
      post.getId(),
      post.getContent(),
      post.getDateTime(),
      post.getPosterId()
    );
  }
}

