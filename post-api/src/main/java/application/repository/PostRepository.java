package application.repository;

import application.domain.Post;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

public interface PostRepository extends CrudRepository<Post, Long> {

  @Query("SELECT p FROM Post p WHERE p.posterId = :posterId")
  public List<Post> findByPosterId(@Param("posterId") Long posterId);

  @Query("SELECT p FROM Post p WHERE p.posterId IN (:ids)")
  public List<Post> findTimeLinePosts(@Param("ids") ArrayList<Long> ids);
}
