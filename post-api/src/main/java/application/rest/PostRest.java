package application.rest;

import application.domain.Post;
import application.dto.PostDto;
import application.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/posts")
public class PostRest {
  @Autowired
  private PostRepository postRepository;

  @GetMapping("/{userId}")
  public ResponseEntity getPostsFromUser(@PathVariable("userId") Long userId) {
    List<PostDto> postDtos = null;
    HttpStatus status = HttpStatus.OK;

    try {
      postDtos = postRepository
        .findByPosterId(userId)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return  ResponseEntity
      .status(status)
      .body(postDtos);
  }

  @GetMapping("/timeline/{userId}")
  public ResponseEntity getTimeLinePostsFromUser(@PathVariable("userId") Long userId, @RequestParam(value = "followers") Long[] followers) {
    ArrayList<Long> ids = new ArrayList<>(Arrays.asList(followers));
    ids.add(userId);

    List<PostDto> postDtos = null;
    HttpStatus status = HttpStatus.OK;

    try {
      postDtos = postRepository
        .findTimeLinePosts(ids)
        .stream()
        .map(PostDto::fromPost)
        .collect(Collectors.toList());
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }
    return  ResponseEntity
      .status(status)
      .body(postDtos);
  }

  @PostMapping()
  public ResponseEntity create(@RequestBody Post post) {
    PostDto postDto = null;
    HttpStatus status = HttpStatus.OK;

    try {
      postDto = PostDto.fromPost(postRepository.save(post));
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity
      .status(status)
      .body(postDto);
  }

  @PutMapping()
  public ResponseEntity update(@RequestBody Post post) {
    PostDto postDto = null;
    HttpStatus status = HttpStatus.OK;

    try {
      postDto = PostDto.fromPost(postRepository.save(post));
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity
      .status(status)
      .body(postDto);
  }

  @DeleteMapping("/{userId}")
  public ResponseEntity delete(@PathVariable("userId") String userId) {
    HttpStatus status = HttpStatus.OK;

    try {
      postRepository.deleteById(Long.valueOf(userId));
    } catch (Exception e) {
      e.printStackTrace();

      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return ResponseEntity.status(status).build();
  }
}
